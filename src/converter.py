from src.consts import *
import math


def is_legal_base(base):
    if not isinstance(base, int):
        return False

    if base > MAX_BASE or base < MIN_BASE:
        return False

    return True


def is_digit(character):
    return ord(character) <= ord(LAST_DIGIT) or ord(character) >= ord(FIRST_CHARACTER)


def alphabet_of_base(base: int):
    if not is_legal_base(base):
        raise ValueError

    if base == MIN_BASE:
        return ['1']

    unfiltered_alphabet = map(lambda x: chr(x), range(ord(FIRST_DIGIT), ord(LAST_CHARACTER) + 1))
    filtered_alphabet = filter(is_digit, unfiltered_alphabet)
    return list(filtered_alphabet)[:base]


def to_upper(char):
    if ord(char) > ord(LAST_CHARACTER):
        return chr(ord(char) - DIFF_BETWEEN_a_A)
    return char


def is_number_legal_in_base(num_as_string, base):
    if not is_legal_base(base):
        raise ValueError

    alphabet = alphabet_of_base(base)
    for digit in num_as_string:
        if not to_upper(digit) in alphabet:
            return False
    return True


def convert_to_decimal(num_as_string, src_base):
    alphabet = alphabet_of_base(src_base)
    converted_dec_number = 0
    counter = 0

    for digit in num_as_string[::-1]:
        converted_dec_number += alphabet.index(to_upper(digit)) * (src_base ** counter)
        counter += 1

    return converted_dec_number


def convert_from_decimal(decimal_number, dst_base):
    alphabet = alphabet_of_base(dst_base)
    converted_num_as_string = ""

    while decimal_number != 0:
        remainder = decimal_number % dst_base
        converted_num_as_string = str(alphabet[remainder]) + converted_num_as_string
        decimal_number = int(decimal_number / dst_base)

    return converted_num_as_string


def convert_base(num_as_string, src_base, dst_base):
    if not is_number_legal_in_base(num_as_string, src_base):
        raise ValueError

    num_as_decimal = convert_to_decimal(num_as_string, src_base)
    num_as_dst_base = convert_from_decimal(num_as_decimal, dst_base)
    return num_as_dst_base


def interactive_converter(num_as_string, src_base, dst_base):
    print(f"Result For ({num_as_string}, {src_base}, {dst_base}):")

    if not is_legal_base(src_base):
        print(f"\tSource base {src_base} is not a legal base")
        return

    if not is_legal_base(dst_base):
        print(f"\tDestination base {dst_base} is not a legal base")
        return

    try:
        result = convert_base(num_as_string, src_base, dst_base)

    except ValueError:
        print(f"\tThe number {num_as_string} Is not a legal number in base {src_base}")
        return

    print(f"\t{num_as_string} is a valid number in base {src_base}")
    print(f"\tIt's representation in destination base ({dst_base}) is: {result}")


