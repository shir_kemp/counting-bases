from src.converter import *


def main():
    interactive_converter('DeadBeef', 16, 10)
    interactive_converter('1PS9wxB', 36, 16)
    interactive_converter('DeadBeef', 15, 10)
    interactive_converter('DeadBeef', 49, 10)


if "__main__" == __name__:
    main()
