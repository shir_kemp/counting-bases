import pytest
from src.converter import *


def test_is_legal_base():
    assert is_legal_base(1) is True
    assert is_legal_base('str') is False
    assert is_legal_base(0) is False
    assert is_legal_base(-6) is False
    assert is_legal_base(37) is False


def test_is_digit():
    assert is_digit('3') is True
    assert is_digit('H') is True
    assert is_digit('@') is False


def test_alphabet_of_base():
    assert alphabet_of_base(1) == ['1']
    assert alphabet_of_base(11) == ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A']
    with pytest.raises(ValueError):
        alphabet_of_base(0)
        alphabet_of_base('a')
        alphabet_of_base(37)


def test_to_upper():
    assert to_upper('a') == 'A'
    assert to_upper('B') == 'B'


def test_is_number_legal_in_base():
    assert is_number_legal_in_base("DeadBeef", 16) is True
    assert is_number_legal_in_base("no", 16) is False


def test_convert_to_decimal():
    assert convert_to_decimal("DeadBeef", 16) == 3735928559
    assert convert_to_decimal("109", 10) == 109


def test_convert_from_decimal():
    assert convert_from_decimal(1234, 16) == '4D2'
    assert convert_from_decimal(150, 2) == '10010110'
    assert convert_from_decimal(16, 17) == 'G'


def test_convert_base():
    assert convert_base("10", 16, 2) == '10000'
    assert convert_base("g", 17, 8) == '20'

